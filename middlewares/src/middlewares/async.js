/**
 * A simple redux middleware that should resolve the promises in the action
 * before it is sent to the next one
 * It can also work as a simple skeleton of redux middlewares
 *
 */
export default function({ dispatch }){
    return next => action => {
        if (!action.payload || !action.payload.then) {
            // ^^^              ^^^ if the action's payload does not have a `then` property
            // ^^^              ^^^ this is our way to figure out if `action.payload` is a promise
            // ^^^ if there's not an action payload
            //|- just pass the action to the next middleware or reducer
            return next(action);
        }

        action.payload
            .then( function(response) {
                //take whatever the original action has and extend the payload
                //with the new content(the returned data) and form the new action
                const newAction = { ...action, payload: response.data};
                //treat newAction as the brand new action and dispatch it to all
                //of the middlewares again!
                dispatch(newAction);
                //^^^ the reason to use `dispatch` here is that:
                //1. we never make assumption about the order of our middleware
                //so that it should work and won't bring other side effects to
                //other middlewares.
                //2. the newAction is actually a NEW action so we want it to go
                //through the middleware loop again.
            });
    }
};
