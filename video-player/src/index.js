import _ from 'lodash';
import React, {Component} from 'react';//create and manage the component
import ReactDOM from 'react-dom'; //manipulating the DOM
import YTSearch from 'youtube-api-search';
//import the SearchBar here
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail.js';


const API_KEY = 'AIzaSyB52cNVozGF7EqRaf-52E9G_AH3RnWnXTs';


class App extends Component{
    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        this.videoSearch('surfboards');
    }

    //define a call back for searching the videos
    videoSearch(term){
        YTSearch({key: API_KEY, term: term}, (videos)=>{
            this.setState({
                videos : videos,
                selectedVideo : videos[0]
            });
        });

    }

    render(){
        //debounce can take the function as the first argument and return a new 
        //  function that can only be called after 300ms
        const videoSearch = _.debounce( (term)=>{
            this.videoSearch(term)
        }, 300);

        return (
            <div>
                <SearchBar onSearchTermChange={videoSearch} />
                <VideoDetail video={this.state.selectedVideo }/>
                {/*passing videos as props */}
                <VideoList
                    onVideoSelect={ selectedVideo => this.setState({selectedVideo})}
                    videos={this.state.videos}/>
            </div>
        );
    }
}

// Take this component's generated HTML and put it on the page (in the DOM)
ReactDOM.render(<App />, document.querySelector('.container')); //<App /> is an instance of Class App
//                       the element that exsisted in the DOM


//Date flow in React:
//  * the data should be handled in the most parent component.
//  * In our case, the components like search_bar, video_detail, video_list and
//      video_list_item all need the data fetched from youtube so their most parent
//      component is App who should be responsible for fetching the data
