//post reducer
import { FETCH_POSTS, FETCH_POST } from '../actions/index.js';

//Thoughts on state:
//  Before we go further, it is ESSENTIAL to have a thorough understanding of
//  the structure of the our state because it is the last thing we want to
//  refactor lator.
//  Here, we are going to have an *array* for holding a list of blog posts and
//  a single obj for holding the detailed blog post
const INITIAL_STATE = { all: [], post: null };

export default function(state=INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_POST:
            return { ...state, post: action.payload.data }
        case FETCH_POSTS:
            return { ...state, all: action.payload.data }
        default:
            return state;
    }
}
