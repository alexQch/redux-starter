import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import App from './components/app';
import Signin from './components/auth/signin';
import Signout from './components/auth/signout';
import Signup from './components/auth/signup';
import Feature from './components/feature';
import Welcome from './components/welcome';
import RequireAuth from './components/auth/require_auth';
import reducers from './reducers';
import reduxThunk from 'redux-thunk';
import { AUTH_USER } from './actions/types';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
//                                                 ^^^^^^
//with reduxThunk middleware, we can return a function as an action with
//`dispatch` function as parameters so that we can dispatch another action
//at some point in the future

const store = createStoreWithMiddleware(reducers);
//    ^^^      ^^^ by pulling store out of the Provider component, React will
//    create this store before anything is rendered so that we can then check
//    the token and set the related key in store accordingly.

const token = localStorage.getItem('token');
// if we have a token, consider the user to be signed in
if (token) {
    // need to update the application state
    store.dispatch({ type: AUTH_USER });
    //    ^^^^ this is the `dispatch` function for sending actions
}

ReactDOM.render(
  <Provider store={store}>
      <Router history={browserHistory}>
          <Route path="/" component={App}>
              <IndexRoute component={Welcome} />
              <Route path="signin" component={Signin} />
              <Route path="signout" component={Signout} />
              <Route path="signup" component={Signup} />
              <Route path="feature" component={RequireAuth(Feature)} />
          </Route>
      </Router>
  </Provider>
  , document.querySelector('.container'));


/*  Thoughts on Automatically sign the user in
    When user is signed in already and after they refresh the page, with the
    current project setup, they will be redirected back to the root page with
    the status of `Not signed in` even though the token is still save in the
    localStorage in the browser.
    To automatically sign in the user based on the saved toekn, we should check
    if the token exsits in the localStorage and validate it if so. The critical
    thing is we need to do this BEFORE the `ReactDOM.render()` method ( before
    any component is rendered ).
*/
