/**
 * local definition of what user is as in mongo database
 *
 */
const bcrypt  = require('bcrypt-nodejs');
var mongoose = require('mongoose');
//define the structure of our models ( fields and etc )
const Schema = mongoose.Schema;

//define our model
const userSchema = new Schema({
    email: { type: String, unique: true, lowercase: true},
    //                      ^^              ^^^ save the email in lowercase
    //                      ^^              ^^^ for uniqueness check
    //                      ^^ make sure the email is unique for every object
    password: String
});

//On save Hook, encrypt password
userSchema.pre('save', function(next) {
    //     ^^   ^^^     ^^^ before saving the model(user), run this function
    const user = this;
    //    ^^^ an instance of the user model which contains email and password

    //generate a salt then run the callback
    bcrypt.genSalt(10, function(err, salt) {
        if (err) {
            return next(err);
        }

        // hash(encrypt) our password using the salt
        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) {
                return next(err);
            }

            //overwrite plain text password with the encrypted version
            user.password = hash;
            next(); //execute the next part: saving the model
        });
    });
});


//define User instance methods: every instance of User class can call these methods
userSchema.methods.comparePassword = function(candidatePassword, callback) {
    //bcrypt will take the candidatePassword and hash it with the salt and
    //then compare it with the user password( also hashed ) and return a
    //boolean value to `isMatch` variable
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        //                                ^^^^^^^^^ the hashed and salted password
        //                                of the user instance that calls this
        //                                function
        if (err) {
            callback(err);
        }
        callback(null, isMatch);
    });
}


//create the model class
//loads the `userSchema` model into `user` collection in mongodb
const ModelClass = mongoose.model('user', userSchema);


//export the model
module.exports = ModelClass;
