/**
 * holds all fo the routing for express
 *
 */
const Authentication = require('./controllers/authentication.js');
const passportService = require('./services/passport.js');
const passport = require('passport');

//a helper function functioning as an interceptor in-between the request and
//the route to perform the authentication
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

//                                                  ^^^^     ^^^
//by default, passport will perform cookie based authentication so that it will
//try to create a new session. Since we use JWT based authentication, no sessions
//are needed.
module.exports =  function(app) {
    app.get('/', requireAuth, function(req, res) {
        //         ^^^^ the middleware to perform the authentication then the
        //         callback function can be run for handling the request
        res.send({
            message: 'super secret code is ABC123'
        });
    });
    app.post('/signin', requireSignin, Authentication.signin);
    app.post('/signup', Authentication.signup);
}
