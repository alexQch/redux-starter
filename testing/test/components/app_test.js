/**
 * The purpose of this file for testing the App component
 *
 */

//import some test helpers
import { renderComponent, expect } from '../test_helper';
//import component we want to test
import App from '../../src/components/app';

// use `describe` to group together similar tests. E.g. App
describe('App', ()=>{
    let component;

    beforeEach( ()=>{
        component = renderComponent(App);
    });

    it("shows a comment box", ()=>{

        expect(component.find('.comment-box')).to.exist;

    });

    it("shows a comment list", ()=>{
        expect(component.find('.comment-list')).to.exist;
    });
});
