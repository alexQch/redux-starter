# Redux Thunk Middleware
Here's the typical flow of a React app with Redux:

      user click something
    |<=======<========<====React=====<======<=========<=======<
    |                                                         |
    =>Action Creator => Action => Middleware => Reducers => State

This work flow work fine with SYNCHRONOUS change of our application where all these steps happens instantly. However, most of the web App we built today need to fetch data in ASYNCHRONOUS channels.

Redux Thunk Middleware can help us with handling ASYNCHRONOUS action creators and ASYNCHRONOUS requests.

Here's an example:
```javascript
import axios from 'axios'

export function fetchUsers(){
    const request = axios.get('http://jsonplaceholder.typicode.com/users');

    //here, instead of returning an action creator, we return a function instead
    return (dispatch)=>{
    //      ^^^^^^^dispatch method of redux
    request.then( ({data})=>{
    //      ^^^ wait until the request is resolved then dispatch an action
        dispatch({type: 'FETCH_USER', payload: data});
    })
    }
}
```
When the action creator is returning a `function` as shown above, the thunk middleware automatically notices that it is an `FUNCTION` rather than an `action`, it will automatically `INVOKE` this method, then after the `request` resolves, it will do `dispatch` as programmed
