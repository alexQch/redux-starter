import { FETCH_WEATHER } from '../actions/index';

export default function(state = [], action) {
    switch (action.type) {
        case FETCH_WEATHER:
            //we never mutate the state here and instead, we should return a new state
            return [ action.payload.data, ...state];
            //nearly identical to return state.concat([action.payload.data]);
            //this will leads to [city, city, city ...]
    }
    return state;
};
