import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

import ReduxPromise from 'redux-promise';

import App from './components/app';
import reducers from './reducers';

//applyMiddleware is used to handle middleware
const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);
//By looking the console, we can see the request object return from the axios get method is
//indeed a promse but after we passing it to the WeatherReducer as payload, its value has
//already been replaced with the response of this promise. This is due to the use of the
//ReduxPromise middleware. When it detects an action's paylaod to be the promise, it will
//stop the action entirely and after the promise resolves, it then sends the new action in
//the same type with the value payload to be the response of that promise.
//In other words, it un-wraps the promise for us.

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <App />
  </Provider>
  , document.querySelector('.container'));
