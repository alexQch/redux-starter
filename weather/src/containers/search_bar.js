import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            term: '' //the value of the input
        };

        /*
            when we pass onInputChange as the onChange callback function without
            bind(this) and when this function is called after user input,
            the this obj in its function will not refer to SearchBar but
            will be UNDEFINED.(remember: this is determined by the caller)
            So, when passing normal function to hanle event, we need to
            use `.bind(this)` or we can use arrow function
        */
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    //handle the change of input
    onInputChange(e){
        this.setState({
            term: e.target.value
        });
    }

    //handle form submission
    onFormSubmit(e){
        //prevent the broser from submitting the form in its default way
        e.preventDefault();

        //send the fetchWeather action
        this.props.fetchWeather(this.state.term);
        //clear the user input
        this.setState({ term: '' });
    }

    render() {
        return (
            /*
                If we don't specify anything, browsers have their own ways of dealing
                with form submision by sending a POST method and re-render the whole
                page. If we are building a single-page-app, such behavior will not be
                appropriate. Instead, we want to handle the form submission inside of
                this app. To do so, we need to prevent the default behavior of the
                form submission using `onSubmit` function handler
            */
            <form className="input-group" onSubmit={this.onFormSubmit}>
                <input
                    placeholder="Get a five-day forecast"
                    className="form-control"
                    value={this.state.term}
                    onChange={this.onInputChange}
                />

                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">
                        Submit
                    </button>
                </span>
            </form>
        );
        //Why use form at all?
        //  1. free extra functionalities like pressing enter the submit...
        //  2. remember to do the preventDefault thing...
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators( { fetchWeather }, dispatch);
}

//since we don't need to map anything from application's state to this Component's props
//we don't need to define the mapStateToProps function and instead, we just pass a null
//as a place holder
export default connect(null, mapDispatchToProps)(SearchBar);
