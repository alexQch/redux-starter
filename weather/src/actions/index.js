import axios from 'axios';

const API_KEY='bed2ba51275c03127cfe1db21cdf7e91'; //key for openweather
const ROOT_URL=`http://api.openweathermap.org/data/2.5/forecast?appId=${API_KEY}`;

//constants for action types
export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
    const url = `${ROOT_URL}&q=${city},us`;

    //the axios get method will return a promise which is then passed as the
    //payload of an action
    const request = axios.get(url);

    return {
        type: FETCH_WEATHER,
        payload: request
    }
}
